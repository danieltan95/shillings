Shillings!
=================

A simple percentage calculator to see where your spending went. 

Currently it only has fixed options, but that feature might be added in the future.
Maybe even make it to be used as a calculator? 

Created as an toy program for javascript.

Technologies used : NodeJS,Jade,JQuery,HTML5/CSS3/JS,Bootstrap,Bootswatch,UglifyJS,SublimeText,GitHub