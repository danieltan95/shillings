/*
function required (valArr){
  var scripthtml="";
  for(x in valArr){
    scripthtml+="<script src="+valArr[x]+"></script>";
  }
  $("body").append(scripthtml);
}
*/
$( document ).ready(function(){
  //load required external scripts, disable when minified.
  //required(["js/util.js","js/chart.js"]);
  formChoiceOnClick();
})

function formChoiceOnClick(){
  var price=$(".form-group>.input-group>input.form-control");
  price.focus(function(){
    createButtons("#buttons",["choice","btn-lg","btn-default"]);
  })
  price.keyup(function(){
    checkNumber(price);
  })
  price.focusout(function(){
    if(checkNumber(price)){
      buttonOnClick(price.val());
    }    
  })
}

function buttonOnClick(value){
  $(".choice").click(function(){
    chart.items[$(this).text()]=parseFloat(value);
    drawProgressBar();
    drawTotalAmount();
  })
  $(".reset").click(function(){
    for(x in chart.items){
      chart.items[x]=0;
    }
    $("#chosen.progress").html("");
    drawTotalAmount();
  })
}
function createButtons(where, classArr){
  var html="";
  var classType=classArr.join(" ");
  for(n in chart.items){
    html+="<span class=\"btn "+classType+"\">"+n+"</span>";
  }
  $(where).hide().html(html).fadeIn();
}

function checkNumber(priceForm){
  var price = priceForm;
  var notNum = price.val().match(/[^\d\.]/) || price.val()==="";
  html = notNum ? "<h3>Numbers only</h3>" : "<h3>Choose type</h3>";
  classType = notNum ? "form-group has-error" : "form-group has-success";
  idType = notNum ? "inputError" : "inputSuccess"
  $(".form-group>label.control-label").html(html);
  $(".form-group").attr("class", classType);
  price.attr("id",idType);
  return !notNum;
}