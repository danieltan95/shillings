//Bunch of utility functions to be used.
function getVal(obj){
  var valList=[];
  for (item in obj){
    valList.push(obj[item]);
  }
  return valList;
}
function sum(arr){
  var sum=0;
  for(x in arr){
    sum+=arr[x];
  }
  return sum;
}
function calcPercent(obj){
  var fin={};
  var total=sum(getVal(obj));
  for(x in obj){
    fin[x]=total?(obj[x]/total)*100 : 0;
  }
  return fin;
}
function zip() {
  var args = [].slice.call(arguments);
  var longest = args.reduce(function(a,b){
    return a.length>b.length ? a : b
  }, []);

  return longest.map(function(_,i){
    return args.map(function(array){return array[i]})
  });
}
function objToArr(thing){
  var itemList=[];
  var propList=[];
  for (item in thing){
    itemList.push(item);
    propList.push(thing[item]);
  }
  return zip(itemList,propList);
}