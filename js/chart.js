/*
Global values for drawing the progress bar. Might get exported as JSON one day.
*/
var chart={
  items:{
    "Food":0,
    "Transport":0,
    "Fun":0,
    "Stationery":0
  }
}

function drawProgressBar(){
  var percentObj=objToArr(calcPercent(chart.items));
  percentObj.sort(function(a, b){return b[1]-a[1]}); //Sort according to the item value.
  var barType=["-info","","-success","-warning","-danger"];
  var html="";
  //percentObj[item][0] is the item name,
  //while percentObj[item][1] is the value
  //Scales the font size according to width
  //from 1em to 2 em, from 25% to 100%
  for(item in percentObj){
    if(percentObj[item][1]>0){
      html+="<div class=\"progress-bar progress-bar"+
            barType[item%barType.length]+
            "\"style=\"width: "+
            percentObj[item][1]+"%; font-size:"+
            (percentObj[item][1]/75 + 2/3)+"em; vertical-align:middle\">"+
            percentObj[item][0]+"("+
            percentObj[item][1].toFixed(2)+"%)</div>";
    }
  }
  $("#chosen.progress").hide().html(html).fadeIn();
}
function drawTotalAmount () {
  var html ="<span class=\"label label-default\">Total Amount Spent : $"+
                    sum(getVal(chart.items)).toFixed(2)+"</span>";
  $("#total").hide().slideUp().html(html).slideDown();
}